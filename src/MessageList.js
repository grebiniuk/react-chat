import React from 'react';
import MessageListItem from './MessageListItem';
import PropTypes from 'prop-types';

class MessageList extends React.Component {
  render() {
    let previousDate;
    const listItems = this.props.data;
    const {doDelete, doLike, updateMessage} = this.props;

    if (listItems) {
      return (
          <ul className="list-group">
            {listItems.map(item => {
              const listItem =
                  <MessageListItem data={item}
                                   doDelete={doDelete}
                                   doLike={doLike}
                                   updateMessage={updateMessage}
                                   key={item.id}
                                   previousDate={previousDate}
                                   className="list-group-item container media"/>;
              previousDate = new Date(item.created_at);

              return listItem;
            })}
          </ul>
      );
    } else {
      return <div className="container">
        <h1>There are no messages yet</h1>
      </div>;
    }
  }
}

MessageList.propTypes = {
  data: PropTypes.array.isRequired,
  updateMessage: PropTypes.func.isRequired,
  doDelete: PropTypes.func.isRequired,
  doLike: PropTypes.func.isRequired,
};

export default MessageList;
