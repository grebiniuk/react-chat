import React from 'react';
import './App.css';
import Chat from './Chat';

class App extends React.Component {
  render() {
    return <Chat/>;
  }
}

export default App;
