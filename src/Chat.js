import React from 'react';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: false,
      error: null,
    };
    this.doDelete = this.doDelete.bind(this);
    this.doLike = this.doLike.bind(this);
    this.onUpdateMessage = this.onUpdateMessage.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('https://api.myjson.com/bins/1hiqin')
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Something went wrong ...');
          }
        })
        .then(data => {
          data.map(item => item.liked = false);
          this.setState({data, isLoading: false});
        })
        .catch(error => this.setState({error, isLoading: false}));
  }

  onAddNewMessage(newMessage) {
    const currentTime = new Date(),
        month = '' + (currentTime.getMonth() + 1),
        day = '' + currentTime.getDate(),
        year = currentTime.getFullYear();
    const data = {
      id: currentTime.getTime().toString(),
      user: 'Me',
      created_at: `${year}-${month}-${day} ${currentTime.toLocaleTimeString()}`,
      message: newMessage,
      marked_read: false,
    };
    this.setState({
      data: [...this.state.data, data],
    });
  }

  onUpdateMessage(id, updateMessage) {
    this.updateMessageItem(id, (data, index) => {
      data[index].message = updateMessage;
    });
  }

  doDelete(id) {
    this.updateMessageItem(id, (data, index) => {
      data.splice(index, 1);
    });
  }

  doLike(id) {
    this.updateMessageItem(id, (data, index) => {
      data[index].like = !data[index].like;
    });
  }

  updateMessageItem(id, updater) {
    const {data} = this.state;
    const index = data.findIndex(item => item.id === id);
    if (index !== -1) {
      updater(data, index);
      this.setState({data});
    }
  }

  render() {
    const {data, isLoading, error} = this.state;

    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <div className="loader">Loading...</div>;
    }

    return (
        <div>
          <div className="clearfix">
            <img src={process.env.PUBLIC_URL + '/img/chat-logo.png'}
                 className="rounded float-left" alt="logo"/>
          </div>

          <Header data={data}/>

          <MessageList data={data}
                       doDelete={this.doDelete}
                       doLike={this.doLike}
                       updateMessage={this.onUpdateMessage}
                       className="container col-sm-3 col-sm-offset-4 frame"/>

          <MessageInput onPushMessage={this.onAddNewMessage.bind(this)}/>
        </div>
    );
  }
}

export default Chat;
