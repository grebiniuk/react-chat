import React from 'react';
import PropTypes from 'prop-types';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.message && state.message === '') {
      state.message = props.message;
    }
    return null;
  }

  onChange(event) {
    this.setState({
      message: event.target.value,
    });
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.onPushMessage(this.state.message);
    this.setState({message: ''});
  }

  render() {
    return (
        <form className="container input-group mb-3 mt-2 p-0"
              onSubmit={this.onSubmit}>
            <textarea className="form-control form-control-lg"
                      placeholder="Write a message"
                      value={this.state.message}
                      onChange={this.onChange}/>
          <input type="submit" className="btn btn-primary" value="Send"/>
        </form>
    );
  }
}

MessageInput.propTypes = {
  message: PropTypes.string,
  onPushMessage: PropTypes.func.isRequired,
};

export default MessageInput;
