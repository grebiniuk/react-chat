import React from 'react';
import MessageInput from './MessageInput';
import PropTypes from 'prop-types';

class MessageListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isEditing: false};

    this.toggleEdit = this.toggleEdit.bind(this);
    this.onPushMessage = this.onPushMessage.bind(this);
  }

  onPushMessage(message) {
    const {data, updateMessage} = this.props;
    updateMessage(data.id, message);
    this.toggleEdit();
  }

  render() {
    const {data} = this.props;
    if (this.state.isEditing) {
      return (
          <MessageInput message={data.message} onPushMessage={this.onPushMessage}/>);
    } else if (data) {
      return (
          <li id={data.id} className="list-group-item container media">
            {this.renderSeparator()}
            <div className="col-sm-1">
              {this.renderAvatar()}
            </div>
            <div className="media-body">
              <h5 className="mt-0">{data.user}</h5>
              <small>{data.created_at}</small>
              <p>{data.message}</p>
              {data.marked_read ?
                  <i className="material-icons float-right">done_all</i> :
                  ''}
              {this.renderActionButtons()}
            </div>
          </li>);
    } else
      return null;
  }

  renderAvatar() {
    const {data} = this.props;
    return data.avatar ?
        (<img src={data.avatar}
              className="mr-3 img-fluid img-thumbnail float-left"
              alt="avatar"/>)
        : null;
  }

  renderSeparator() {
    const {data, previousDate} = this.props;
    const currentDate = new Date(data.created_at);
    return (
        previousDate &&
        (previousDate.getFullYear() !== currentDate.getFullYear() ||
            previousDate.getMonth() !== currentDate.getMonth() ||
            previousDate.getDate() !== currentDate.getDate())
    ) ?
        (<div className="new-date">{currentDate.toDateString()}
          <hr/>
        </div>)
        : null;
  }

  toggleEdit() {
    this.setState({isEditing: !this.state.isEditing});
  }

  renderActionButtons() {
    const {data, doDelete, doLike} = this.props;
    if (data.user === 'Me') {
      return (
          <div>
            <i className="material-icons float-right"
               onClick={this.toggleEdit}>edit</i>
            <i className="material-icons float-right"
               onClick={event => doDelete(data.id)}>delete</i>
          </div>);
    } else {
      return (<i className={`material-icons float-right${data.like ? ' btn-success' : ''}`}
                 onClick={event => doLike(data.id)}>thumb_up</i>);
    }

  }
}

MessageListItem.propTypes = {
  previousDate: PropTypes.instanceOf(Date),
  data: PropTypes.object.isRequired,
  updateMessage: PropTypes.func.isRequired,
  doDelete: PropTypes.func.isRequired,
  doLike: PropTypes.func.isRequired,
};

export default MessageListItem;
