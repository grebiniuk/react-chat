import React from 'react';

class Header extends React.Component {
  render() {
    const listItems = this.props.data;
    if (listItems) {
      let messagesCount = listItems.length;
      let usersCount = listItems.reduce((uniqueUsers, item) => {
        if (uniqueUsers.indexOf(item.user) === -1) {
          uniqueUsers.push(item.user);
        }
        return uniqueUsers;
      }, []).length;
      let lastMessageTime = new Date(Math.max(...listItems.map(function(item) {
        return new Date(item.created_at);
      })));

      return (
          <header className="Chat-header container bg-info text-white">
            <div className="row">

              <h1 className="text-center col-sm-3">Friends' chat</h1>

              <div className="participants-number col-sm-3 text-center my-auto">
                {usersCount} participants
              </div>

              <div className="messages-number col-sm-3 text-center my-auto">
                {messagesCount} messages
              </div>

              <div className="last-message col-sm-3 text-right my-auto">
                last message at {lastMessageTime.getHours()}:{lastMessageTime.getMinutes()}
              </div>

            </div>
          </header>);
    } else {
      return (
          <header className="Chat-header">
            <h1>Can't read the information about this chat</h1>
          </header>
      );
    }
  }
}

export default Header;
